<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\User\UserRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function getinfo($social)
    {
        return $this->userRepository->getinfo($social);
    }
    public function checkinfo($social)
    {
        $this->userRepository->checkinfo($social);
        return redirect()->route('home');
    }
    public function logout()
    {
        $this->userRepository->logout();
        return redirect()->route('home');
    }
}
