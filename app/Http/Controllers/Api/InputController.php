<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Search\SearchRepositoryInterface;
use Illuminate\Http\Request;
use App\Services\Search\SearchServices;

class InputController extends Controller
{
    protected $postservice;

    public function __construct(SearchServices $searchService)
    {
        $this->searchService = $searchService;
    }

    public function input(Request $request)
    {

        $result =$this->searchService->input($request);

        return response()->json([
                    "result" => $result,
                ]);
    }
    public function history($id)
    {

        $data = $this->searchService->history($id);

        return response()->json([
                "data" => $data,
            ]);
    }
}
