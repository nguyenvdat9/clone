<?php

namespace App\Services\Search;

use Illuminate\Http\Request;
use App\Repositories\Search\SearchRepositories;
use Illuminate\Support\Facades\RateLimiter;
use App\Models\Search;
class SearchServices
{
    public function __construct(SearchRepositories $searchRepositories)
    {
        $this->searchRepositories = $searchRepositories;
    }

    public function input(Request $request)
    {
        $user_id = $request->user_id;
        $key = 'input'.$request->ip();
        $input_content = $request->input_content;
        $search_old = $this->searchRepositories->search_old($input_content);

        if(isset($search_old)){
            $search_id = $search_old->id;
            $user_old = $this->searchRepositories->user_old($user_id,$search_id);
            if($user_old==null && $user_id != null){
                $array_history = array([
                        'user_id' => $user_id,
                        'search_id' => $search_id,
                    ]);
                    $this->searchRepositories->create_history($array_history);
                }
                $result = array([
                    'input_content' => $search_old->input_content,
                    'output_content' => $search_old->output_content,
                    'key'=>$key,
                    'retries'=>RateLimiter::retriesLeft($key, 3),
                    'seconds'=>RateLimiter::availableIn($key),
                ]);        
                return $result;
        }else{
            $curl = curl_init();
            $url = "https://api.kuroshiro.org/convert";
            $a = array("str" => "$input_content", "to" => "hiragana", "mode" => "normal", "romajiSystem" => "nippon");
            $data = http_build_query($a);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, $data);

        // thiet lap cac du lieu gui di
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $html = curl_exec($curl);
        if ($e = curl_error($curl)) {
            echo $e;
        } else {
            $decoded = json_decode($html);
            foreach ($decoded as $key => $val) {
                $output_content = $val;
            }
        }
        curl_close($curl);
        // check user
        $search =  Search::create([
            'input_content'=>$input_content,
            'output_content'=>$output_content,
        ]);
        if(isset($user_id)){
                $search_id = $search->id;
                
                $array_history = array([
                    'user_id' => $user_id,
                    'search_id' => $search_id,
                ]);
                $this->searchRepositories->create_history($array_history);
        }
      
        $result = array([
            'input_content' => $input_content,
            'output_content' => $output_content,
            'key'=>$key,
            'retries'=>RateLimiter::retriesLeft($key, 3),
            'seconds'=>RateLimiter::availableIn($key),
        ]);
        
        return $result;
            
       
        }
       
    }
    public function history($id){
        return $this->searchRepositories->find_history($id);
    }
}