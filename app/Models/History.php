<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;
    protected $table = 'history';
    protected $fillable = ['user_id', 'search_id'];
    public function search(){
        return $this->hasMany(Search::class,'id', 'search_id');
    }
    public function user(){
        return $this->hasOne(User::class,'id', 'user_id');
    }
}
