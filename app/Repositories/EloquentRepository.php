<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;

abstract class EloquentRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $search;
    protected $history;
    protected $user;

    /**
     * EloquentRepository constructor.
     */
    public function __construct()
    {
        $this->setModelSearch();
        $this->setModelHistory();
        $this->setModelUser();
    }

    /**
     * get model
     * @return string
     */
    abstract public function getModelSearch();
    abstract public function getModelHistory();
    abstract public function getModelUser();
    /**
     * Set model
     */
    public function setModelSearch()
    {
        $this->search = app()->make(
            $this->getModelSearch()
        );
    }
    public function setModelHistory()
    {
        $this->history = app()->make(
            $this->getModelHistory()
        );
    }
    public function setModelUser()
    {
        $this->user = app()->make(
            $this->getModelUser()
        );
    }

  

}