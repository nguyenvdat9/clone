<?php
namespace App\Repositories\User;


use Illuminate\Support\Carbon;
use App\Models\History;
use App\Models\Search;
use App\Models\User;
use App\Repositories\EloquentRepository;
use Exception;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class UserEloquentRepository extends EloquentRepository implements UserRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModelSearch()
    {
        return Search::class;
    }
    public function getModelHistory()
    {
        return History::class;
    }
    public function getModelUser()
    {
        return User::class;
    }
    public function getinfo($social)
    {
        return Socialite::driver($social)->redirect();
    }
    public function checkinfo($social)
    {
        try {
            $info = Socialite::driver($social)->user();
            // dd($info);
            $finduser = $this->user::where('facebook_id', $info->id)->first();
            
            if ($finduser) {
                Auth::login($finduser);
            } else {
                $newUser = $this->user::create(
                    [
                        'name' => $info->name,
                        'facebook_id' => $info->id,
                    ]
                );

                Auth::login($newUser);
                
            }
        } catch (Exception $e) {
            echo "error";
        }
    }
    public function logout()
    {
        Auth::logout();
        
    }
    
}