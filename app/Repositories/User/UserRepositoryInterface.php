<?php

namespace App\Repositories\User;
use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    /**
     * Get all
     * @return mixed
     */
    public function getinfo($social);

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function checkinfo($social);
    public function logout();
}