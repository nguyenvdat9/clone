<?php
namespace App\Repositories\Search;


use Illuminate\Support\Carbon;
use App\Models\History;
use App\Models\Search;
use App\Models\User;
use App\Repositories\EloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;



class SearchRepositories
{
    protected $search;

    public function __construct(Search $search,History $history)
    {
        $this->search = $search;
        $this->history = $history;

    }
    public function search_old($input_content)
    {
       return $this->search->where('input_content','=',"$input_content")->first();
    }
    public function user_old($user_id,$search_id)
    {
       return $this->history->where('user_id',$user_id)->where('search_id',$search_id)->first();
    }
    public function create_history(array $array_history)
    {
       return $this->history->insert($array_history);
    }
    public function find_history($id)
    {
       return $this->history->join('search', 'search.id', '=', 'history.search_id')->orderBy('history.created_at','DESC')->where('user_id', '=', "$id")->limit(9)->get();
    }
}