<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return view('welcome');
})->name('home');


// Route::post('input_content','Api\InputController@input')->name('input');
Route::get('history/{id}', 'Api\InputController@history')->name('history');

Route::get('/getinfo_fb/{social}','Api\SocialController@getinfo')->name('getinfo');
Route::get('/checkinfo_fb/{social}','Api\SocialController@checkinfo')->name('checkinfo');
Route::get('account-logout', 'Api\SocialController@logout')->name('logout');


    Route::post('input_content', 'Api\InputController@input')->name('input')->middleware('throttle:input');



