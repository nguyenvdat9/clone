<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class Search extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('input_content')->unique();
            $table->text('output_content');
            $table->timestamps();
            
        });
        DB::statement('ALTER TABLE `search` ADD FULLTEXT INDEX input_index (input_content)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('search', function($table) {
            $table->dropIndex('input_index');
        });
        Schema::dropIfExists('search');
    }
}
