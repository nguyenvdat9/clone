@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ url('public/translate.css') }}">
@endsection
@section('main')

    <div class="container">
        <div class="text-center">
            <div class="row">
                @if (Auth::check())
                    <div class="dropdown">

                        <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Hi! {{ Auth::user()->name }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="triggerId">
                            <a href="{{ route('logout') }}">
                                Logout
                            </a>

                        </div>

                    </div>

                @else

                    <button type="button" class="btn btn-info btn-round" data-toggle="modal" data-target="#loginModal">
                        Login
                    </button>

                @endif

            </div>
            <div class="row">
                <div class="text-center">
                    <div class="col-md-12">
                        <h3>Translate Kanji => Hiragana</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="demoArea">
                        <label>Original Text:</label>
                       
                            @if (Auth::check())
                                <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->facebook_id }}">
                            @endif
                            <div>
                                <textarea id="input_content" name="input_content"
                                    style="width:100%;border-radius: .2em;font-size: 1em;" rows="4">
                                        </textarea>
                            </div>
                            <br>
                            <div class="nut_convert">
                                <button id="submit" class="btn btn-default">Convert</button>
                            </div>
                        
                        <div>
                            <p id="output_content">

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    @if (Auth::check())
                        {{-- {{ route('history', Auth::user()->facebook_id) }} --}}
                        <a id="show-data" href="javascript:void(0)" data-id="{{ Auth::user()->facebook_id }}"
                            class="btn btn-info">
                            History
                        </a>

                        <div class="row thu_4">
                            <table>
                                <thead>
                                    <div id="head">
                                        <th id="input">input</th>
                                        <th></th>
                                        <th id="output">output</th>
                                    </div>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    @endif
                </div>
            </div>



            <script>
                $(document).ready(function() {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    
                    $('body').on('click','#submit',function(e) {
                        var input = $('#input_content').val();
                    var id = $('#user_id').val();
                        $.ajax({
                            url: "api/input_content",
                            type: "POST",
                            data: {
                                input_content:input,
                                user_id:id,
                            },
                            success: function(response) {
                                $.each(response.result, function(key, item) {
                                    $("#input_content").html(item.input_content);
                                    $("#output_content").html(item.output_content);
                                //     var retries = item.retries;
                                //     var seconds = item.seconds;
                                //     if (retries <= 0) {
                                //     alert("Too Many Request, please try again" + seconds + " seconds");
                                // }
                                });
                            }
                        });
                    });

                    // history

                    $('#show-data').click(function(e) {
                        var user_id = $(this).data('id');
                        $.ajax({
                            url: "api/history/" + user_id,
                            type: "GET",
                            // dataType: "json",
                            success: function(response) {
                                var resultData = response.data;
                                $('tbody').html("");
                                $.each(resultData, function(key, item) {
                                    $('tbody').append('<tr>\
                                                        <td>' + item.input_content + '</td>\
                                                        <td>' + '<i class="fa fa-arrow-right" aria-hidden="true"></i>' + '</td>\
                                                        <td>' + item.output_content + '</td>\
                                                        </tr>');
                                });
                            }
                        });
                    });
                });
            </script>
        </div>


    </div>






    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-title text-center">
                        <h4>Login</h4>
                    </div>
                    <div class="d-flex flex-column text-center">
                        <div class="text-center text-muted delimiter">use a social network</div>
                        <div class="d-flex justify-content-center social-buttons">
                            <a href="{{ route('getinfo', ['facebook']) }}">
                                <i class="fab fa-facebook"></i>
                            </a>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <div class="signup-section">Not a member yet?</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{-- end login --}}
    {{-- $(document).ready(function(){

    $('#country_name').keyup(function(){ //bắt sự kiện keyup khi người dùng gõ từ khóa tim kiếm
     var query = $(this).val(); //lấy gía trị ng dùng gõ
     if(query != '') //kiểm tra khác rỗng thì thực hiện đoạn lệnh bên dưới
     {
      var _token = $('input[name="_token"]').val(); // token để mã hóa dữ liệu
      $.ajax({
       url:"{{ route('search') }}", // đường dẫn khi gửi dữ liệu đi 'search' là tên route mình đặt bạn mở route lên xem là hiểu nó là cái j.
       method:"POST", // phương thức gửi dữ liệu.
       data:{query:query, _token:_token},
       success:function(data){ //dữ liệu nhận về
        $('#countryList').fadeIn();  
        $('#countryList').html(data); //nhận dữ liệu dạng html và gán vào cặp thẻ có id là countryList
      }
    });
    }
  }); --}}
@endsection
